FROM openjdk:8-jre-alpine
COPY build/libs/*.jar /opt/giveaway-bot/lib/app.jar
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "/opt/giveaway-bot/lib/app.jar"]
