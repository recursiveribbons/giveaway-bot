package dev.robinsyl.giveaway.services;


import dev.robinsyl.giveaway.types.BotUser;
import dev.robinsyl.giveaway.types.Giveaway;
import dev.robinsyl.giveaway.types.GiveawayEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.User;

import javax.annotation.PreDestroy;
import java.io.Closeable;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * A service for interfacing with the database. Requires a PostgreSQL database.
 */
@Service
public class DatabaseService implements Closeable {

    private final Logger logger = LoggerFactory.getLogger(DatabaseService.class);
    private final Connection con;

    /**
     * A constructor of DatabaseService with wired Spring values for externalised configuration.
     * The constructor will attempt 10 tries at connecting, with increasing timeouts, as Docker may not have started the database yet.
     *
     * @param url      The JDBC string
     * @param user     The database username
     * @param password The database password
     */
    @Autowired
    public DatabaseService(@Value("${giveaway.db.url}") String url, @Value("${giveaway.db.user}") String user, @Value("${giveaway.db.password}") String password) {
        int tries = 1;
        int timeout = 500;
        Connection connection;
        while (true) {
            try {
                connection = DriverManager.getConnection(url, user, password);
                break;
            } catch (SQLException e) {
                logger.warn("Could not get connection, try {}", tries);
            }
            try {
                //noinspection BusyWait
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                logger.error("Thread interrupted", e);
                Thread.currentThread().interrupt();
            }
            timeout *= 2;
            if (tries++ > 10) {
                throw new BeanInitializationException("Could not get connection, giving up.");
            }
        }
        this.con = connection;
        logger.info("Connected to database");
        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate("create table users(id int constraint users_pk primary key, first_name varchar(32) not null, last_name varchar(32), username varchar(32), language_code varchar(16), steam varchar(32), verified boolean default false not null, creator boolean default false not null, chance smallint default 2 not null, last_updated timestamp default current_timestamp not null, joined timestamp default current_timestamp not null);");
            stmt.executeUpdate("create table giveaways(id serial constraint giveaways_pk primary key, name varchar(64) not null, creator_id int not null constraint giveaways_users_id_fk references users, active boolean default false not null, start_time timestamp default current_timestamp not null, end_time timestamp not null, keytype smallint not null, gamekey text not null, winner_id int constraint giveaways_users_winner_fk_2 references users);");
            stmt.executeUpdate("create table in_giveaway(user_id int constraint in_giveaway_users_id_fk references users, giveaway_id int constraint in_giveaway_giveaways_id_fk references giveaways, join_time timestamp default current_timestamp not null, constraint in_giveaway_pk primary key (user_id, giveaway_id));");
            stmt.executeUpdate("create table notes(id serial constraint notes_pk primary key, type smallint default 1 not null, sender_id int not null constraint notes_users_id_sender_fk references users, recipient_id int not null constraint notes_users_id_recipient_fk references users, body text, send_time timestamp default current_timestamp not null);");
            stmt.executeUpdate("create table counters(name varchar(16) constraint counters_pk primary key, counter int default 0);");
            logger.info("Database has been initialised.");
        } catch (SQLException e) {
            throw new BeanInitializationException("Could not initialise database", e);
        }
    }

    /**
     * Closes the database connection
     */
    @PreDestroy
    public void close() {
        try {
            con.close();
        } catch (SQLException e) {
            logger.info("Could not close connection", e);
        }
    }

    // Giveaway functionality

    /**
     * Creates a new giveaway in the database, and returns the result.
     *
     * @param giveaway The giveaway to create
     * @return An {@link Optional<Giveaway>} with the inserted giveaway, empty if unsuccessful.
     */
    public Optional<Giveaway> newGiveaway(Giveaway giveaway) {
        Optional<Giveaway> result = Optional.empty();
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO giveaways VALUES(name, creator_id, active, start_time, end_time, keytype, gamekey);", Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(1, giveaway.getName());
            stmt.setInt(2, giveaway.getCreatorId());
            stmt.setBoolean(3, giveaway.isActive());
            stmt.setObject(4, giveaway.getStart());
            stmt.setObject(5, giveaway.getEnd());
            stmt.setInt(6, giveaway.getKeyType().getValue());
            stmt.setString(7, giveaway.getKey());
            int affectedRows = stmt.executeUpdate();
            if (affectedRows <= 0) {
                return Optional.empty();
            }
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    int id = rs.getInt("id");
                    result = getGiveaway(id);
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in newGiveaway!", e);
            return Optional.empty();
        }
        return result;
    }

    /**
     * Converts a database result for a giveaway to a {@link Giveaway} object
     *
     * @param rs A {@link ResultSet} resulting from a query with every single field from {@link Giveaway}.
     * @return The giveaway represented by the result set.
     * @throws SQLException SQL exception
     */
    private Giveaway resultSetToGiveaway(ResultSet rs) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        int creatorId = rs.getInt("creator_id");
        boolean active = rs.getBoolean("active");
        LocalDateTime start = rs.getObject("start_time", LocalDateTime.class);
        LocalDateTime end = rs.getObject("end_time", LocalDateTime.class);
        Giveaway.KeyType keyType = Giveaway.KeyType.of(rs.getInt("keytype"));
        String key = rs.getString("gamekey");
        Integer winnerId = rs.getObject("winner_id", Integer.class);
        return new Giveaway(id, name, creatorId, active, start, end, keyType, key, winnerId);
    }

    /**
     * Fetches a giveaway with a particular ID from the database
     *
     * @param giveawayId The id of the giveaway
     * @return An {@link Optional} with the giveaway with the given id if found, empty if not or error.
     */
    public Optional<Giveaway> getGiveaway(int giveawayId) {
        Giveaway giveaway;
        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name, creator_id, active, start_time, end_time, keytype, gamekey, winner_id FROM giveaways WHERE id=?;")) {
            stmt.setInt(1, giveawayId);
            boolean result = stmt.execute();
            if (!result) {
                return Optional.empty();
            }
            try (ResultSet rs = stmt.getResultSet()) {
                if (!rs.next()) {
                    return Optional.empty();
                }
                giveaway = resultSetToGiveaway(rs);
            }
        } catch (SQLException e) {
            logger.error("Database error in getGiveaway!", e);
            return Optional.empty();
        }
        return Optional.of(giveaway);
    }

    /**
     * Returns a list of active giveaways, which are ones that have not been raffled yet.
     *
     * @return A list of active giveaways, empty if none or error
     */
    public Set<Giveaway> getActiveGiveaways() {
        Set<Giveaway> giveaways = new HashSet<>();
        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name, creator_id, active, start_time, end_time, keytype, gamekey, winner_id FROM giveaways WHERE active=1;")) {
            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        giveaways.add(resultSetToGiveaway(rs));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in getActiveGiveaways!", e);
            return Collections.emptySet();
        }
        return giveaways;
    }

    /**
     * Returns a list of giveaways whose end time are less than 5 minutes away, and can be raffled.
     *
     * @return A list of giveaways to be raffled, empty if none or error.
     */
    public List<Giveaway> getRaffleGiveaways() {
        List<Giveaway> giveaways = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement("SELECT id, name, creator_id, active, start_time, end_time, keytype, gamekey, winner_id FROM giveaways WHERE active=1 AND (end_time - CURRENT_TIMESTAMP) <= INTERVAL '5 minutes';")) {
            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        giveaways.add(resultSetToGiveaway(rs));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in getRaffleGiveaways!", e);
            return Collections.emptyList();
        }
        return giveaways;
    }

    /**
     * Gets the entries for a giveaway, for use in a raffle.
     *
     * @param giveawayId The giveaway to fetch entries from
     * @return A {@link List} of the users who have entered a giveaway, along with the chance with those users, wrapped in a {@link GiveawayEntry} object. Empty if none or error.
     */
    public List<GiveawayEntry> getGiveawayEntries(int giveawayId) {
        List<GiveawayEntry> giveawayEntries = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement("SELECT user_id, chance FROM in_giveaway i INNER JOIN users u ON u.id = i.user_id WHERE giveaway_id=? AND u.verified=1 AND u.chance>0;")) {
            stmt.setInt(1, giveawayId);

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        int userId = rs.getInt("user_id");
                        int chance = rs.getInt("chance");
                        giveawayEntries.add(new GiveawayEntry(userId, chance));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in getGiveawayEntries!", e);
            return Collections.emptyList();
        }
        return giveawayEntries;
    }

    /**
     * Sets the winner of a giveaway, and sets the giveaway to inactive.
     *
     * @param giveawayId The giveaway to set
     * @param winnerId   The winner of the giveaway
     */
    public void setGiveawayWinner(int giveawayId, int winnerId) {
        try (PreparedStatement stmt = con.prepareStatement("UPDATE giveaways SET winner_id=?, active=0 WHERE id=?;")) {
            stmt.setInt(1, winnerId);
            stmt.setInt(2, giveawayId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in setGiveawayWinner!", e);
        }
    }

    // User functionality

    /**
     * This methods returns a user in the database with the given user ID.
     *
     * @param userId The ID of the user
     * @return An {@link Optional<BotUser>} with the user if found, empty if not found or error.
     */
    public Optional<BotUser> getUser(int userId) {
        BotUser user;
        try (PreparedStatement stmt = con.prepareStatement("SELECT first_name, last_name, username, language_code, steam, verified, creator, chance FROM users WHERE id=?;")) {
            stmt.setInt(1, userId);
            boolean result = stmt.execute();
            if (!result) {
                return Optional.empty();
            }
            try (ResultSet rs = stmt.getResultSet()) {
                if (!rs.next()) {
                    return Optional.empty();
                }
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                String username = rs.getString("username");
                String languageCode = rs.getString("language_code");
                boolean verified = rs.getBoolean("verified");
                String steam = rs.getString("steam");
                int chance = rs.getInt("chance");
                boolean creator = rs.getBoolean("creator");
                user = new BotUser(userId, firstName, lastName, username, languageCode, verified, steam, chance, creator);
            }
        } catch (SQLException e) {
            logger.error("Database error in getUser!", e);
            return Optional.empty();
        }
        return Optional.of(user);
    }

    /**
     * This methods sets the chance of a user to win a giveaway.
     *
     * @param userId The user whose chance to set
     * @param chance The weighted chance for a user to win a giveaway. For a normal user, this defaults to 2.
     */
    public void setUserChance(int userId, int chance) {
        try (PreparedStatement stmt = con.prepareStatement("UPDATE users SET chance=? WHERE id=?;")) {
            stmt.setInt(1, chance);
            stmt.setInt(2, userId);
        } catch (SQLException e) {
            logger.error("Database error in setUserChance!", e);
        }
    }

    /**
     * Resets the chance for users to 2 if they have not won any giveaways in the past two months.
     */
    public void resetUserChances() {
        try (Statement stmt = con.createStatement()) {
            stmt.execute("UPDATE users u SET chance=2 WHERE NOT EXISTS (SELECT g.winner_id FROM giveaways g WHERE (CURRENT_TIMESTAMP - g.end_time) < INTERVAL '2 months' AND u.id=g.winner_id);");
        } catch (SQLException e) {
            logger.error("Database error in resetUserChances!", e);
        }
    }

    /**
     * This method adds a user to the database. If the user already exists, the user data will be updated.
     *
     * @param user The user to add or update
     */
    public void addOrUpdateUser(User user) {
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO users(id, first_name, last_name, username, language_code) VALUES(?,?,?,?,?) ON CONFLICT (id) DO UPDATE SET first_name=?, last_name=?, username=?, language_code=?, last_updated=current_timestamp;")) {
            // INSERT
            stmt.setInt(1, user.getId());
            stmt.setString(2, user.getFirstName());
            stmt.setString(3, user.getLastName());
            stmt.setString(4, user.getUserName());
            stmt.setString(5, user.getLanguageCode());
            // ON CONFLICT UPDATE
            stmt.setString(6, user.getFirstName());
            stmt.setString(7, user.getLastName());
            stmt.setString(8, user.getUserName());
            stmt.setString(9, user.getLanguageCode());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in addOrUpdateUser!", e);
        }
    }

    // Stats

    /**
     * Gets the number of users in the database
     *
     * @return The number of users, 0 if none or error.
     */
    public int getUserCount() {
        int result = 0;
        try (Statement stmt = con.createStatement()) {
            stmt.execute("SELECT count(*) AS num FROM users;");
            try (ResultSet rs = stmt.getResultSet()) {
                if (rs.next()) {
                    result = rs.getInt("num");
                }
            }
        } catch (SQLException e) {
            return 0;
        }
        return result;
    }

    /**
     * Deletes all user data for a particular user.
     *
     * @param userId ID of user whose data to delete
     */
    public void removeUser(int userId) {
        try (PreparedStatement stmt = con.prepareStatement("DELETE FROM users WHERE id=?;")) {
            stmt.setInt(1, userId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in removeUser", e);
        }
    }
}
