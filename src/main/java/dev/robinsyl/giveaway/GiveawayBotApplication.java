package dev.robinsyl.giveaway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GiveawayBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(GiveawayBotApplication.class, args);
    }

}
