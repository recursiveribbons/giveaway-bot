package dev.robinsyl.giveaway.bots;

import dev.robinsyl.giveaway.services.DatabaseService;
import dev.robinsyl.giveaway.types.BotUser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.objects.Ability;
import org.telegram.abilitybots.api.objects.Flag;
import org.telegram.abilitybots.api.objects.MessageContext;
import org.telegram.abilitybots.api.objects.Reply;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChatMember;
import org.telegram.telegrambots.meta.api.methods.groupadministration.LeaveChat;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.ChatMember;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static dev.robinsyl.giveaway.bots.KeyboardBuilder.inline;
import static java.util.Objects.nonNull;
import static org.telegram.abilitybots.api.objects.Flag.CALLBACK_QUERY;
import static org.telegram.abilitybots.api.objects.Flag.INLINE_QUERY;
import static org.telegram.abilitybots.api.objects.Locality.USER;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;
import static org.telegram.abilitybots.api.util.AbilityUtils.getChatId;
import static org.telegram.abilitybots.api.util.AbilityUtils.getUser;

@Component
public class GiveawayBot extends AbilityBot {

    private final DatabaseService db;
    private final MessageSource messageSource;
    private final int CREATOR_ID;
    private final long MAIN_CHAT;

    protected GiveawayBot(DatabaseService db, MessageSource messageSource, @Value("${giveaway.bot.token}") String botToken, @Value("${giveaway.bot.username}") String botUsername, @Value("${giveaway.bot.creator.id}") int creatorId, @Value("${giveaway.chat.main.id}") long mainChatId) {
        super(botToken, botUsername);
        this.messageSource = messageSource;
        this.db = db;
        CREATOR_ID = creatorId;
        MAIN_CHAT = mainChatId;
        silent.send(getString("bot.restarted"), creatorId());
    }

    @Override
    public int creatorId() {
        return CREATOR_ID;
    }

    // Commands

    @SuppressWarnings("unused")
    public Ability start() {
        return Ability.builder()
                .name("start")
                .info("Start the bot")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    User user = ctx.user();
                    if (db.getUser(user.getId()).map(BotUser::isVerified).orElse(false)) {
                        sendString("bot.welcome.verified", ctx);
                    } else {
                        db.addOrUpdateUser(user);
                        sendString("bot.welcome.verify.help", ctx);
                    }
                })
                .build();
    }

    @SuppressWarnings("unused")
    public Ability cancel() {
        return Ability.builder()
                .name("cancel")
                .info("Cancel any commands")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> super.db.<Long, Integer>getMap("user_state_replies").remove(ctx.chatId()))
                .build();
    }

    // Replies

    @SuppressWarnings("unused")
    public Reply processCallbackQuery() {
        Consumer<Update> action = u -> {
            String response = getString("bot.query.processed", u);
            CallbackQuery query = u.getCallbackQuery();
            User user = query.getFrom();
            AnswerCallbackQuery answer = new AnswerCallbackQuery()
                    .setCallbackQueryId(query.getId())
                    .setText(response);
            silent.execute(answer);
        };
        return Reply.of(action, CALLBACK_QUERY);
    }

    @SuppressWarnings("unused")
    public Reply greetUsers() {
        Consumer<Update> action = u -> u.getMessage().getNewChatMembers().forEach(user -> {
            if (db.getUser(user.getId()).isPresent()) {
                SendMessage sendMessage = new SendMessage(MAIN_CHAT, getString("bot.welcome", u, user.getFirstName()))
                        .setReplyMarkup(inline()
                                .buttonUrl(getString("bot.welcome.rules.button", u), getString("bot.welcome.rules.url"))
                                .buttonUrl(getString("bot.welcome.verify.button", u), getString("bot.welcome.verify.url"))
                                .build());
                silent.execute(sendMessage);
            } else {
                sendString("bot.return", u);
            }
            db.addOrUpdateUser(user);
        });
        return Reply.of(action, Flag.MESSAGE, Flags.NEW_CHAT_MEMBERS);
    }

    @SuppressWarnings("unused")
    public Reply leaveChats() {
        Consumer<Update> action = u -> {
            if (u.getMessage().getNewChatMembers().stream().anyMatch(cm -> cm.getUserName().equals(getBotUsername()))) {
                LeaveChat leaveChat = new LeaveChat();
                leaveChat.setChatId(u.getMessage().getChatId());
                silent.execute(leaveChat);
            }
        };
        return Reply.of(action, Flag.MESSAGE, Flags.NEW_CHAT_MEMBERS);
    }

    @Override
    public boolean checkGlobalFlags(Update update) {
        return Flag.MESSAGE.test(update) || INLINE_QUERY.test(update) || CALLBACK_QUERY.test(update);
    }

    // Convenience methods

    private int getUserId(Update update) {
        return getUser(update).getId();
    }

    boolean isInChat(int userId) {
        Collection<String> validStatuses = Arrays.asList("creator", "administrator", "member");
        GetChatMember getChatMember = new GetChatMember()
                .setChatId(MAIN_CHAT)
                .setUserId(userId);
        Optional<ChatMember> chatMember = silent.execute(getChatMember);
        return chatMember.map(ChatMember::getStatus)
                .map(validStatuses::contains)
                .orElse(false);
    }

    boolean isAdmin(int userId) {
        Collection<String> validStatuses = Arrays.asList("creator", "administrator");
        GetChatMember getChatMember = new GetChatMember()
                .setChatId(MAIN_CHAT)
                .setUserId(userId);
        Optional<ChatMember> chatMember = silent.execute(getChatMember);
        return chatMember.map(ChatMember::getStatus)
                .map(validStatuses::contains)
                .orElse(false);
    }

    // Convenience methods related to strings and sending strings

    private void sendHTML(String msg, long chatId) {
        SendMessage sendMessage = new SendMessage(chatId, msg)
                .enableHtml(true);
        silent.execute(sendMessage);
    }

    void sendUserMessage(String key, int userId, Object... args) {
        sendHTML(getString(key, args), userId);
    }

    private void sendUserMessage(String key, User user, Object... args) {
        sendHTML(getString(key, user, args), user.getId());
    }

    void sendAnnouncement(String key, Object... args) {
        sendHTML(getString(key, args), MAIN_CHAT);
    }

    private void sendString(String key, MessageContext ctx, Object... args) {
        sendHTML(getString(key, ctx, args), ctx.chatId());
    }

    private void sendString(String key, Update update, Object... args) {
        sendHTML(getString(key, update, args), getChatId(update));
    }

    private String getString(String key, MessageContext ctx, Object... args) {
        return getString(key, ctx.user(), args);
    }

    private String getString(String key, Update update, Object... args) {
        return getString(key, getUser(update), args);
    }

    String getString(String key, User user, Object... args) {
        return getString(key, user.getLanguageCode(), args);
    }

    String getString(String key, Object... args) {
        return getString(key, (String) null, args);
    }

    private String getString(String key, String localeString, Object... args) {
        if (localeString == null) {
            return messageSource.getMessage(key, args, Locale.ENGLISH);
        }
        return messageSource.getMessage(key, args, new Locale(localeString));
    }

    long getMainChat() {
        return MAIN_CHAT;
    }

    private enum Flags implements Predicate<Update> {

        NEW_CHAT_MEMBERS(u -> u.getMessage().getNewChatMembers() != null && !u.getMessage().getNewChatMembers().isEmpty());

        private final Predicate<Update> predicate;

        Flags(Predicate<Update> predicate) {
            this.predicate = predicate;
        }

        public boolean test(Update update) {
            return nonNull(update) && predicate.test(update);
        }
    }
}
