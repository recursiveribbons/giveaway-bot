package dev.robinsyl.giveaway.bots;

import dev.robinsyl.giveaway.services.DatabaseService;
import dev.robinsyl.giveaway.types.BotUser;
import dev.robinsyl.giveaway.types.Giveaway;
import dev.robinsyl.giveaway.types.GiveawayEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class RaffleComponent {

    private final Logger logger = LoggerFactory.getLogger(DatabaseService.class);
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private final Random random = new Random();

    private final GiveawayBot bot;
    private final DatabaseService db;

    @Autowired
    public RaffleComponent(GiveawayBot bot, DatabaseService db) {
        this.bot = bot;
        this.db = db;
    }

    @Scheduled(cron = "00 * * * *")
    public void raffle() {
        db.resetUserChances();
        Collection<Giveaway> giveaways = db.getRaffleGiveaways();
        // Loop through each giveaway
        for (Giveaway giveaway : giveaways) {
            List<GiveawayEntry> entries = db.getGiveawayEntries(giveaway.getId());
            if (entries.isEmpty()) {
                // Giveaway is empty
                bot.sendAnnouncement("bot.raffle.empty");
                continue;
            }
            // Create weighted participants list using their chance
            List<Integer> weightedParticipants = new ArrayList<>();
            for (GiveawayEntry entry : entries) {
                for (int i = 0; i < entry.getChance(); i++) {
                    weightedParticipants.add(entry.getUserId());
                }
            }

            Optional<BotUser> winnerOptional = findWinner(weightedParticipants);
            if (!winnerOptional.isPresent()) {
                // Ran out of iterations to find a winner
                bot.sendAnnouncement("bot.raffle.unable");
                continue;
            }
            BotUser winner = winnerOptional.get();
            db.setGiveawayWinner(giveaway.getId(), winner.getId());
            db.setUserChance(winner.getId(), 1);
            notifyWinner(giveaway, winner);
            bot.sendUserMessage("bot.raffle.creator", giveaway.getCreatorId(), winner.getDisplayString(), giveaway.getName());
            bot.sendAnnouncement("bot.raffle.announcement", winner.getDisplayString(), giveaway.getName());
            // Notifiy all participants
            scheduler.scheduleWithFixedDelay(new NotifyParticipants(giveaway, entries), 0, 1, TimeUnit.SECONDS);
        }
    }

    private Optional<BotUser> findWinner(List<Integer> weightedParticipants) {
        Integer winner = null;
        // Maximum 10 iterations for finding winner
        for (int i = 0; i < 10; i++) {
            winner = weightedParticipants.get(random.nextInt(weightedParticipants.size()));
            if (bot.isInChat(winner)) {
                // Winner was found
                break;
            }
            // Found invalid winner, set to null and try again
            winner = null;
        }
        return Optional.ofNullable(winner).flatMap(db::getUser);
    }

    private void notifyWinner(Giveaway giveaway, BotUser winner) {
        int giveawayId = giveaway.getId();
        InlineKeyboardMarkup keyboard = KeyboardBuilder.inline()
                .button(bot.getString("bot.raffle.winner.button.thank", winner), "codethank " + giveawayId).row()
                .button(bot.getString("bot.raffle.winner.button.broken", winner), "codefail " + giveawayId).row()
                .buttonUrl(bot.getString("bot.raffle.winner.button.tip", winner), bot.getString("bot.donate.button.link", winner))
                .build();
        String link = getGiveawayLink(giveaway, winner);
        String winnerMsg = bot.getString("bot.raffle.winner.notification", winner, giveawayId, giveaway.getName(), link);
        SendMessage message = new SendMessage((long) winner.getId(), winnerMsg)
                .enableHtml(true)
                .setReplyMarkup(keyboard);
        try {
            bot.execute(message);
        } catch (TelegramApiException e) {
            logger.error("Unable to notify winner", e);
        }
    }

    private String getGiveawayLink(Giveaway giveaway, User user) {
        switch (giveaway.getKeyType()) {
            case STEAM_KEY:
                return bot.getString("bot.giveaway.key.format.steam", user, giveaway.getKey());
            case HUMBLE_KEY:
                return bot.getString("bot.giveaway.key.format.humble", user, giveaway.getKey());
            case OTHER_KEY:
            default:
                return giveaway.getKey();
        }
    }

    private class NotifyParticipants implements Runnable {

        private final Giveaway giveaway;
        private final List<GiveawayEntry> entries;

        private NotifyParticipants(Giveaway giveaway, List<GiveawayEntry> entries) {
            this.giveaway = giveaway;
            this.entries = entries;
        }

        @Override
        public void run() {
            if (entries.isEmpty()) {
                throw new RuntimeException("Finished sending mass messages for giveaway " + giveaway.getId());
            }
            GiveawayEntry entry = entries.remove(0);
            bot.sendUserMessage("bot.raffle.participant", entry.getUserId(), giveaway.getName(), giveaway.getId());
        }
    }
}
