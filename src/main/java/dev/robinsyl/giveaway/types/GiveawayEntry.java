package dev.robinsyl.giveaway.types;

import java.util.Objects;

public class GiveawayEntry {
    private final int userId;
    private final int chance;

    public GiveawayEntry(int userId, int chance) {
        this.userId = userId;
        this.chance = chance;
    }

    public int getUserId() {
        return userId;
    }

    public int getChance() {
        return chance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiveawayEntry that = (GiveawayEntry) o;
        return userId == that.userId &&
                chance == that.chance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, chance);
    }
}
