package dev.robinsyl.giveaway.types;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

public class Giveaway {

    private final Integer id;
    private final String name;
    private final int creatorId;
    private final boolean active;
    private final LocalDateTime start;
    private final LocalDateTime end;
    private final KeyType keyType;
    private final String key;
    private final Integer winnerId;

    public Giveaway(String name, int creatorId, LocalDateTime start, LocalDateTime end, KeyType keyType, String key) {
        this.id = null;
        this.name = name;
        this.creatorId = creatorId;
        this.active = true;
        this.start = start;
        this.end = end;
        this.keyType = keyType;
        this.key = key;
        this.winnerId = null;
    }

    public Giveaway(Integer id, String name, int creatorId, boolean active, LocalDateTime start, LocalDateTime end, KeyType keyType, String key, Integer winnerId) {
        this.id = id;
        this.name = name;
        this.creatorId = creatorId;
        this.active = active;
        this.start = start;
        this.end = end;
        this.keyType = keyType;
        this.key = key;
        this.winnerId = winnerId;
    }

    public GiveawayBuilder builder() {
        return new GiveawayBuilder();
    }

    public GiveawayBuilder builder(int creatorId, String name) {
        return new GiveawayBuilder(creatorId, name);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public boolean isActive() {
        return active;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public KeyType getKeyType() {
        return keyType;
    }

    public String getKey() {
        return key;
    }

    public Integer getWinnerId() {
        return winnerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Giveaway giveaway = (Giveaway) o;
        return creatorId == giveaway.creatorId &&
                active == giveaway.active &&
                Objects.equals(id, giveaway.id) &&
                Objects.equals(name, giveaway.name) &&
                Objects.equals(start, giveaway.start) &&
                Objects.equals(end, giveaway.end) &&
                keyType == giveaway.keyType &&
                Objects.equals(key, giveaway.key) &&
                Objects.equals(winnerId, giveaway.winnerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, creatorId, active, start, end, keyType, key, winnerId);
    }

    public enum KeyType {
        STEAM_KEY(1), HUMBLE_KEY(2), OTHER_KEY(3);

        private final int value;

        KeyType(int value) {
            this.value = value;
        }

        public static KeyType of(int value) {
            switch (value) {
                case 1:
                    return STEAM_KEY;
                case 2:
                    return HUMBLE_KEY;
                default:
                    return OTHER_KEY;
            }
        }

        public int getValue() {
            return value;
        }
    }

    public static class GiveawayBuilder {

        private String name;
        private int creatorId;
        private Duration duration;
        private KeyType keyType;
        private String key;

        private GiveawayBuilder() {
        }

        public GiveawayBuilder(int creatorId, String name) {
            this.creatorId = creatorId;
            this.name = name;
        }

        public Giveaway build() {
            LocalDateTime start = LocalDateTime.now();
            LocalDateTime end = start.plus(duration);
            end.toEpochSecond(ZoneOffset.UTC);
            return new Giveaway(name, creatorId, start, end, keyType, key);
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setCreatorId(int creatorId) {
            this.creatorId = creatorId;
        }

        public void setDuration(Duration duration) {
            this.duration = duration;
        }

        public void setKeyType(KeyType keyType) {
            this.keyType = keyType;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }
}
