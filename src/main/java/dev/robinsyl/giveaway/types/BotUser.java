package dev.robinsyl.giveaway.types;

import org.telegram.telegrambots.meta.api.objects.User;

import java.util.Objects;

public class BotUser extends User {
    private final boolean verified;
    private final String steam;
    private final int chance;
    private final boolean creator;

    public BotUser(Integer id, String firstName, String lastName, String userName, String languageCode, boolean verified, String steam, int chance, boolean creator) {
        super(id, firstName, false, lastName, userName, languageCode);
        this.verified = verified;
        this.steam = steam;
        this.chance = chance;
        this.creator = creator;
    }

    public boolean isVerified() {
        return verified;
    }

    public String getSteam() {
        return steam;
    }

    public int getChance() {
        return chance;
    }

    public boolean isCreator() {
        return creator;
    }

    public String getDisplayString() {
        StringBuilder name = new StringBuilder(getFirstName());
        if (getLastName() != null) {
            name.append(' ').append(getLastName());
        }
        if (getUserName() != null) {
            name.append(" (@").append(getUserName()).append(')');
        }
        return name.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BotUser botUser = (BotUser) o;
        return verified == botUser.verified &&
                chance == botUser.chance &&
                creator == botUser.creator &&
                Objects.equals(steam, botUser.steam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), verified, steam, chance, creator);
    }
}
